from numpy.lib.npyio import save
import pandas as pd
import numpy as np
import genshinstats
import configparser
from pdb import set_trace as bp
import datetime
import os

def get_all_charas(config):
    data_chara = genshinstats.get_characters(config['UID']['uid'])
    #TODO prettifier

def get_spiral_abyss(config):  
    data_abyss = genshinstats.get_spiral_abyss(config['UID']['uid'])
    #TODO prettifier

def get_basic_stats(config):
    data_basic = genshinstats.get_user_stats(config['UID']['uid'])
    #TODO prettifier

def get_daily_reward(config):
    claimed, day = genshinstats.get_daily_reward_info()
    if claimed:
        print(f'You have claimed your day {day} reward.\nLast 10 login rewards are:')
        for idx, x in enumerate(genshinstats.get_claimed_rewards()):
            print(f"Claimed: {x['created_at']} | Reward: {x['name']} | Count: {x['cnt']}")
            if idx == 9: break
        return
    else:
        reward = genshinstats.claim_daily_reward()
        if reward is not None:
            claimed, day = genshinstats.get_daily_reward_info()
            print(f"You claimed your day {day} reward of {reward['cnt']} {reward['name']}")
            print("Here is your latest 10 rewards:")
            for idx, x in enumerate(genshinstats.get_claimed_rewards()):
                print(f"Claimed: {x['created_at']} | Reward: {x['name']} | Count: {x['cnt']}")
                if idx == 9: break
            return
        else:
            print('oof :v')
    input('\n\nPress any key to continue...')

def get_artifact_log(config):
    artifact_log = genshinstats.get_artifact_log()
    #TODO prettify

def get_primogem_log(config):
    primogem = genshinstats.get_primogem_log()
    get, spent = (0,0)
    for i in primogem:

        if i['amount'] > 0 : 
            print(f'Aquired {i["amount"]} gems -- {i["reason"]}')
            get+=i['amount']
        else: 
            print(f"Spent {i['amount']} gems -- {i['reason']}")
            spent += -1*(i['amount'])
    
    print(f"Total since {i['time']} | get: {get} | spent: {spent}\n\n")
    input('\n\nPress any key to continue...')

def get_wish_history(config, banner_to_fetch:str, save_path:str):
    if os.path.isfile(save_path):
        _wish_data = pd.read_csv(save_path, index_col=0)
        _time_guard = datetime.datetime.strptime(_wish_data.iloc[-1].time, '%Y-%m-%d %H:%M:%S')
    else:
        _wish_data = pd.DataFrame(columns=['time','type','rarity','name'])
        _time_guard = datetime.datetime(year=2000,month=1,day=1)
    _banner_type = genshinstats.get_banner_types()
    _wish_id = [x for x  in _banner_type.keys() if banner_to_fetch.lower() in _banner_type[x].lower()]
    _wish_id = _wish_id[0]

    _wish_history = genshinstats.get_wish_history(_wish_id)
    _wish_history = reversed(list(_wish_history))
    for x in _wish_history:
        if datetime.datetime.strptime(x['time'], '%Y-%m-%d %H:%M:%S') > _time_guard:
            _wish_data = _wish_data.append({'time':x['time'], 'type':x['type'], \
                'rarity':x['rarity'], 'name':x['name']}, ignore_index=True)
    _wish_data['rarity'] = pd.to_numeric(_wish_data['rarity'])
    _wish_data.to_csv(save_path)
    print(f'Data saved to {save_path}.')
    input('\n\nPress any key to continue...')

def wish_analysis(config):
    _wish_path = input('enter the .csv path\n')
    print(f'Processing {_wish_path}')
    _wish_data = pd.read_csv(_wish_path,index_col=0)
    _five_star_pity_count = []
    _four_star_pity_count = []
    _five_star_pity = 0
    _four_star_pity = 0

    for idx, row in _wish_data.iterrows():
        _five_star_pity += 1
        _four_star_pity += 1
        if row['rarity'] == 5:
            _five_star_pity_count.append((row['type'],row['name'], _five_star_pity))
            _five_star_pity = 0
        elif row['rarity'] == 4:
            _four_star_pity_count.append((row['type'],row['name'], _four_star_pity))
            _four_star_pity = 0
    
    print(f'Processing {idx+1} pull since {_wish_data.iloc[0].time}.')
    print(f'Approximately, {(idx+1)*160} primogems were used.')

    _total = idx+1
    _five_star_total = len(_five_star_pity_count)
    _four_star_total = len(_four_star_pity_count)
    _three_star_total = _total - (_five_star_total+_four_star_total)

    print('5* data:')
    _avg = 0
    for idx, x in enumerate(_five_star_pity_count):
        print(f'Got {x[0]} {x[1]} on {x[2]} pull')
        _avg += x[2]
    print(f'Average pity: {_avg/(idx+1)}')
    print(f'You are now on {_five_star_pity} pull or \n{90-_five_star_pity} (character banner) / {80-_five_star_pity} (weapon banner) away from hard pity\n\n')

    print('4* data:')
    _avg = 0
    for idx, x in enumerate(_four_star_pity_count):
        print(f'Got {x[0]} {x[1]} on {x[2]} pull')
        _avg += x[2]
    print(f'Average pity: {_avg/(idx+1)}\n\n')
    input('Press any key to continue...')
    #TODO graph and prettifier

def get_resin_log(config):
    resin = genshinstats.get_resin_log()
    get, spent = (0,0)
    first_time = None
    for i in resin:
        if first_time is None:
            #  '2021-01-01 00:07:49'
            first_time = datetime.datetime.strptime(i['time'], '%Y-%m-%d %H:%M:%S')
        if i['amount'] > 0 : 
            print(f'Aquired {i["amount"]} resin -- {i["reason"]}')
            get+=i['amount']
        else: 
            print(f"Spent {i['amount']} resin -- {i['reason']}")
            spent += -1*(i['amount'])
    
    last_time = datetime.datetime.strptime(i['time'], '%Y-%m-%d %H:%M:%S')
    diff = last_time-first_time
    diff = np.abs(diff.days)*24*3600 + diff.seconds
    natural_resin = diff/8/60
    refresh = int((spent - natural_resin)/60)*60
    print(f"Total since {i['time']} | refresh: {refresh} | spent: {spent}\n\n")
    input('Press any key to continue...')

def get_crystal_log(config):
    crystal = genshinstats.get_crystal_log()
    get, spent = (0,0)
    for i in crystal:
        if i['amount'] > 0 : 
            print(f'Aquired {i["amount"]} crystal -- {i["reason"]}')
            get+=i['amount']
        else: 
            print(f"Spent {i['amount']} crystal -- {i['reason']}")
            spent += -1*(i['amount'])

    print(f"Total since {i['time']} | get: {get} | spent: {spent}\n\n")
    input('Press any key to continue...')

if __name__ == '__main__':
    config = configparser.RawConfigParser()
    config.read('token_personal.cfg')

    genshinstats.set_cookie(ltuid=config['WEB_TOKEN']['ltuid'], \
        ltoken=config['WEB_TOKEN']['ltoken'])
    genshinstats.set_authkey(genshinstats.extract_authkey(config['AUTH_KEY']['auth_key']))

    print('-----------------------------------------')
    print('---welcome to genshin stats downloader---')
    print('-----------------------------------------')
    print('please make sure that you have input\nyour info in token.cfg correctly')

    while True:
        print('What do you want to do:\n \
            1. Get all characters\n \
            2. Get spiral abyss data\n \
            3. Get basic stats\n \
            4. Get daily rewards\n \
            5. Get artifact log \n \
            6. Get primogem log \n \
            7. Get resin log\n \
            8. Get crystal log\n \
            9. Get character wish log\n \
            10. Get weapon wish log\n \
            11. Get standard wish log\n \
            12. Analyze wish data')

        #TODO wish history

        choice = int(input('which one?:\n'))
        
        if choice == 1:
            get_all_charas(config)

        elif choice == 2:
            get_spiral_abyss(config)

        elif choice == 3:
            get_basic_stats(config)

        elif choice == 4:
            get_daily_reward(config)

        elif choice == 5:
            get_artifact_log(config)

        elif choice == 6:
            get_primogem_log(config)

        elif choice == 7:
            get_resin_log(config)

        elif choice == 8:
            get_crystal_log(config)

        elif choice == 9:
            get_wish_history(config, 'Character','character_wish.csv')
        
        elif choice == 10:
            get_wish_history(config, 'Weapon','weapon_wish.csv')

        elif choice == 11:
            get_wish_history(config, 'Permanent','permanent_wish.csv')

        elif choice == 12:
            wish_analysis(config)

        else:
            pass